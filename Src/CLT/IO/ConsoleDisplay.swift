import Foundation
import Model

/// Console displayer of the app
public class ConsoleDisplay : Displayable {
    /// Display board
    ///
    /// - Parameter board : board to display
    public func displayBoard(board: Board) {
        print(board)
    }
}
