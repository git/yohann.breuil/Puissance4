import Foundation
import Model

func insertPiece(id : Int, column : Int, _ board : inout Board) {
    let result = board.insertPiece(id: id, column: column)
    switch result{
    case .ok:
        print("OK")
    case .failed(let reason):
        switch reason{
        case .columnFull:
            print("Column \(column) is full")
        case .boardFull:
            print("Board \(column) is full")
        case .unknown:
            print("Unknown")
        case .negativeOrOutOfBound:
            print("Row or column must be posittive")
        }
    default:
        print("Rien")
    }
    print(board)
}

if var board = Board(withNbRows: 6, withNbColumns: 7) {
    print(board)
    insertPiece(id: 1, column: 0, &board)
    insertPiece(id: 1, column: 0, &board)
    insertPiece(id: 1, column: 0, &board)
    insertPiece(id: 2, column: 3, &board)
    insertPiece(id: 2, column: 1, &board)
    insertPiece(id: 2, column: 0, &board)
    insertPiece(id: 2, column: 0, &board)
    insertPiece(id: 2, column: 0, &board)
    insertPiece(id: 2, column: 0, &board)
    insertPiece(id: 2, column: 0, &board)
    insertPiece(id: 2, column: 0, &board)
    insertPiece(id: 2, column: 0, &board)
    
    _ = board.removePiece(column: 0)
    _ = board.removePiece(column: 0)
    _ = board.removePiece(column: 8)
    
    print(board)
}

var grid : [[Int?]] = [[1,2,nil],[nil,nil,nil],[nil,nil,nil]]

var b = Board(withGrid: grid)

var displayer = ConsoleDisplay()
displayer.displayBoard(board: b!)
