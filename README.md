# Connect 4

## Rules

* Game has 1 grid, 2 players 
* Each player choose one column and insert a piece 
* The first player to connect 4 checkers in a row is the winner.

## Getting started

### Requirements 

* Swift
* XCode

### Get project 

```sh
git clone https://codefirst.iut.uca.fr/git/yohann.breuil/Puissance4.git
```

### Open in XCode

```sh
cd Src
open PuissanceQuatre.xcworkspace
```

### Build model  

```sh
cd Src/Model
swift build
``` 

### Run tests

```sh
cd Src/Model
swift test
``` 

### Launch app (in XCode) 

> Click on Arrow icon

> **Warning**
> Game is no finished but there is code unused but with logic in comments

### Documentation (open in XCode)

```sh
cd Doc
open Model.doccarchive
```

## Author

* Yohann BREUIL
