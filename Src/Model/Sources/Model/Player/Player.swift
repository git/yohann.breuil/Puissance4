import Foundation

/// Player of Connect 4 game
/// 
/// - Author: Yohann BREUIL
public class Player {
    /// Counter for unique id
    private static var idCounter : Int = 1
    
    /// Player id
    public var id : Int
    
    /// Player nickname
    public var nickname : String
    
    /// Initialize player with nickname
    ///
    /// - Parameter nickname : nickname to attrubute to player
    public init(nickname: String) {
        self.id = Player.idCounter + 1
        self.nickname = nickname
    }
    
    /// Choose a column
    ///
    /// - Returns : id of column
    public func chooseColumn() -> Int {
        return 1
    }
}
