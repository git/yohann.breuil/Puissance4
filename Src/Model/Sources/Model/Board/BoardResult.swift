import Foundation

/// The result  of an action on board
///
/// - Author: Yohann BREUIL
public enum BoardResult : Equatable {
    case unknow
    case ok
    case failed(reason : FailedResult)
}

/// The result of a failed action on board
///
/// - Author: Yohann BREUIL
public enum FailedResult {
    case unknown
    case negativeOrOutOfBound
    case columnFull
    case boardFull
}
