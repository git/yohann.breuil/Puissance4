import XCTest
import Model

final class HumanTests: XCTestCase {
    func testInit() {
        func expect(nickname : String) {
            func scan() -> Int { return 1 }
            
            let human = Human(nickname: nickname, scanner: scan)
            
            XCTAssertEqual(human.nickname, nickname)
        }
        
        expect(nickname: "yobreuil")
        expect(nickname: "cebouhou")
        expect(nickname: "macheval")
    }
}
