import Foundation

/// Artificial intelligence player of Connect 4 game
///
/// - Author: Yohann BREUIL
public class AI : Player {
    /// Initialize the artificial intelligence
    ///
    /// Artificial intelligence nickname was "AI" by default
    public init() {
        super.init(nickname: "AI")
    }
    
    /// Choose a column
    ///
    /// AI return a random number
    ///
    /// - Returns : id of column
    public override func chooseColumn() -> Int {
        return 1
    }
}
