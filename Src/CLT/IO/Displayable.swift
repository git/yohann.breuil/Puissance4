import Foundation
import Model

/// Displayer of app
public protocol Displayable {
    /// Display board
    ///
    /// - Parameter board : board to display
    func displayBoard(board: Board)
}
