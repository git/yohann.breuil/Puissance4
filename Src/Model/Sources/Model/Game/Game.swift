import Foundation

/// Game of Connect 4
///
/// - Author: Yohann BREUIL
public class Game {
    /// Rules of the game
    private var rules : Rules
    
    /// Board of the game
    private var board : Board?
    
    /// Players of the game
    private var players : [Player] = []
    
    /// Initialize game with a rule
    public init(rules: Rules) {
        self.rules = rules
        self.board = createBoard(rules: rules)
    }
    
    /// Create board from rules values of number of rows and columns
    ///
    /// - Parameter rules : Rules of the game to create board
    ///
    /// - Returns : created board of the game
    public func createBoard(rules: Rules) -> Board? {
        Board(withNbRows: type(of: rules).nbRows, withNbColumns: type(of: rules).nbColumns) ?? nil
    }
    
    /// Insert a piece in board
    ///
    /// - Parameter player : player which insert a piece
    public func insertPiece(player: Player) {

    }
}
