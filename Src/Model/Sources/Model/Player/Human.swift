import Foundation

/// Human player of Connec 4 game
///
/// - Author: Yohann BREUIL
public class Human : Player {
    private var scanner : () -> Int
    
    /// Initialize human player with a nickname and a scanner method
    ///
    /// - Parameter nickname : nickname to add to player
    /// - Parameter scanner : scanner method which return an integer
    public init(nickname: String, scanner: @escaping () -> Int) {
        self.scanner = scanner
        super.init(nickname: nickname)
    }
    
    /// Initialize human player with  a scanner method
    ///
    /// Human nickname was "Player" by default 
    ///
    /// - Parameter scanner : scanner method which return an integer
    public convenience init(scanner: @escaping () -> Int) {
        self.init(nickname: "Player", scanner: scanner)
    }
    
    /// Choose a column
    ///
    /// Human use a scanner to choose id
    ///
    /// - Returns : id of column
    public override func chooseColumn() -> Int {
        scanner()
    }
}
