import Foundation

/// Rules of Connect 4 game
///
/// - Author: Yohann BREUIL
public protocol Rules {
    /// Number of rows in the grid
    static var nbRows : Int {get}
    
    /// Number of columns in the grid
    static var nbColumns : Int {get}
    
    /// Number of alignedPieces to align to win
    static var nbAlignedPieces : Int {get}
    
    /// Number of trials before lose game
    static var nbTrials : Int {get}
     
    /// Create board with values of number of rows and columns
    ///
    /// - Returns : created board
    func createBoard() -> Board?
    
    /// Check if game is over
    ///
    /// - Returns : true if game is over else false, coordinates of last piece, game result
    func isGameOver() -> (Bool, (Int, Int), GameResult)
    
    /// Returns id of next palyer to play
    ///
    /// The player who must play is the one who has the fewest tokens
    ///
    /// - Parameter board : board game
    ///
    /// - Returns: id of next palyer to play
    func getNextPlayer(board: Board) -> Int
}
