/// Cclassic rules of Connect 4 game
///
/// - Author : Yohann BREUIL
public class ClassicRules : Rules {
    /// number of rows
    public static var nbRows: Int = 6
    
    /// number of columns
    public static var nbColumns: Int = 7
    
    /// number of aligned piece to win
    public static var nbAlignedPieces: Int = 4
    
    /// number of trials before lose by disqualification
    public static var nbTrials: Int = 3
    
    /// Coordinates of winning pieces
    public var winCoord : [(Int, Int)] = []

    /// Create board with values of number of rows and columns
    ///
    /// - Returns : created board
    public func createBoard() -> Board? {
        Board(withNbRows: Self.nbRows, withNbColumns: Self.nbColumns) ?? nil
    }
    
    /// Check if game is over
    ///
    /// - Returns : true if game is over else false, coordinates of last piece, game result
    public func isGameOver() -> (Bool, (Int, Int), GameResult) {
        return (true, (1,2), .lose);
    }

    /// Returns id of next palyer to play
    ///
    /// The player who must play is the one who has the fewest tokens
    ///
    /// - Parameter board : board game
    ///
    /// - Returns: id of next palyer to play
    public func getNextPlayer(board: Board) -> Int {
        var countP1 = 0
        var countP2 = 0
        
        for row in 0..<board.nbRows {
            for col in 0..<board.nbColumns {
                if board.grid[row][col] == 1 {
                    countP1 = countP1 + 1
                }
                if board.grid[row][col] == 2 {
                    countP2 = countP2 + 2
                }
            }
        }
        
        return countP1 > countP2 ? 2 : 1
    }
    
    /// Check if 4 pieces were aligned in vertical
    ///
    /// - Parameter board : board to examine
    /// - Parameter coord  : coordinates of last point
    ///
    /// - Returns : true if 4 pieces were aligned in vertical else false
    private func checkVertical(board: Board, coord: (Int, Int)) -> Bool {
        return true
    }
    
    /// Check if 4 pieces were aligned in horizontal
    ///
    /// - Parameter board : board to examine
    /// - Parameter coord  : coordinates of last point
    ///
    /// - Returns : true if 4 pieces were aligned in horizontal else false
    private func checkHorizontal(board: Board, coord: (Int, Int)) -> Bool {
        return true
    }
    
    /// Check if 4 pieces were aligned in diagonal
    ///
    /// - Parameter board : board to examine
    /// - Parameter coord  : coordinates of last point
    ///
    /// - Returns : true if 4 pieces were aligned in diagonal else false
    private func checkDiagonal(board: Board, coord: (Int, Int)) -> Bool {
        return true
    }
}
