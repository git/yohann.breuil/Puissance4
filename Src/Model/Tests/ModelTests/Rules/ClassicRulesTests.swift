import XCTest
import Model

final class ClassicRulesTests: XCTestCase {
    func testStaticValues() {
        XCTAssertEqual(6, ClassicRules.nbRows)
        XCTAssertEqual(7, ClassicRules.nbColumns)
        XCTAssertEqual(4, ClassicRules.nbAlignedPieces)
        XCTAssertEqual(3, ClassicRules.nbTrials)
    }
}
