import Foundation

/// The board of the Connect 4 game
///
/// - Author: Yohann BREUIL
public struct Board : CustomStringConvertible {
    /// Number of rows in the board
    public let nbRows : Int
    
    /// Number of columns in the board
    public let nbColumns : Int
    
    /// String description of the grid
    public var description: String {
        var string = String()
        
        for row in grid.reversed(){
            for cell in row {
                string.append("|")
                string.append("\(String(describing: Board.descriptionMapper[cell] ?? "-"))")
            }
            string.append("|\n")
        }
        
        return string
    }
    
    private static let descriptionMapper : [Int? : String] = [nil : "-", 1 : "X", 2 : "O"]
    
    var grid : [[Int?]]
    
    public var gridBoard : [[Int?]] {
        grid
    }
    
    /// Initialize the board with number of rows and number of columns
    ///
    /// - Parameter nbRows : The number of rows of the board
    /// - Parameter nbColumns : The number of culumns of the board
    ///
    public init?(withNbRows nbRows : Int, withNbColumns nbColumns : Int) {
        guard nbRows > 0 && nbColumns > 0 else { return nil }
        
        self.nbRows = nbRows
        self.nbColumns = nbColumns
        
        grid = Array.init(repeating: Array.init(repeating: nil, count: nbColumns), count: nbRows)
    }
    
    /// Initialize the board with a grid
    /// 
    /// - Parameter grid : The grid of the board
    public init?(withGrid grid : [[Int?]]) {
        let sizes = grid.map { return $0.count }
                
        let result = sizes.allSatisfy {$0 == grid[0].count }
               
        guard result else { return nil }
        
        nbRows = grid.count
        nbColumns = grid[0].count
        self.grid=grid
    }
    
    /// Verify if a column of the grid is full
    ///
    /// - Parameter column : The column index to verify
    ///
    /// - Returns: True if the column is full else false
    private func isColumnFull(column : Int) -> Bool{
        for row in 0..<nbRows{
            if grid[row][column] == nil{
                return false
            }
        }
        return true
    }
    
    /// Verify if the board is full
    ///
    /// - Returns: True if the board  is full else false
    public func isFull() -> Bool{
        for column in 0..<nbColumns{
            if !isColumnFull(column: column){
                return false
            }
        }
        return true
    }

    /// Insert a piece into the grid
    ///
    /// - Parameter id : The id of the player
    /// - Parameter row : The row  to add the piece
    /// - Parameter column : The column to add the piece
    /// - Returns: The result of the insertion in a `BoardResult`
    private mutating func insertPiece(id : Int, row : Int, column : Int) -> BoardResult {
        guard row >= 0 && row < nbRows && column >= 0 && column < nbColumns else {
            return .failed(reason: .negativeOrOutOfBound)
        }
        guard grid[row][column] == nil else{
            return .failed(reason: .columnFull)
        }
        grid[row][column] = id
        return .ok
    }

    /// Insert a piece into the grid
    ///
    /// - Parameter id : The id of the player
    /// - Parameter column : The column to add the piece
    /// - Returns: The result of the insertion in a `BoardResult`
    public mutating func insertPiece(id : Int, column:Int) -> BoardResult {
        guard column >= 0 && column < nbColumns else {
            return .failed(reason: .negativeOrOutOfBound)
        }
        
        for row in 0..<nbRows {
            if (grid[row][column] == nil) {
                return insertPiece(id: id, row: row, column: column)
            }
        }
        return .failed(reason: .columnFull)
    }

    /// Remove a piece from the grid
    ///
    /// - Parameter row : The row  to remove the piece
    /// - Parameter column : The column to remove the piece
    ///
    /// - Returns: True if the piece can be removed else false
    public mutating func removePiece(column : Int) -> Bool {
        guard column >= 0 && column < nbRows else {
            return false
        }
        
        for row in stride(from: nbRows - 1, through: 0, by: -1) {
            if grid[row][column] != nil{
                grid[row][column] = nil
                return true
            }
        }
        return true
    }
}
