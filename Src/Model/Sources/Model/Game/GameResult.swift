import Foundation

/// Represents result of a game
///
/// - Author : Yohann BREUIL
public enum GameResult {
    case lose
}
