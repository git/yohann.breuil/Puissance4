import Foundation

/// Reader of app
public protocol Readable {
    /// Read and return an integer value
    func readInt() -> Int
}
