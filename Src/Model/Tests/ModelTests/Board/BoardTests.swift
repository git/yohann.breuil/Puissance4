import XCTest
import Model

final class BoardTest: XCTestCase {
    func testInitWithVaalues() throws {
        func expect(nbRows : Int, nbColumns : Int, notNil : Bool) {
            let board = Board(withNbRows: nbRows, withNbColumns: nbColumns)

            if !notNil {
                XCTAssertNil(board)
                return
            }
            XCTAssertNotNil(board)
            XCTAssertEqual(board?.nbRows, nbRows)
            XCTAssertEqual(board?.nbColumns, nbColumns)
        }

        expect(nbRows: 6, nbColumns: 7, notNil: true)
        expect(nbRows: 0, nbColumns: 7, notNil: false)
        expect(nbRows: 6, nbColumns: 0, notNil: false)
        expect(nbRows: 0, nbColumns: 0, notNil: false)
        expect(nbRows: -2, nbColumns: 7, notNil: false)
        expect(nbRows: 6, nbColumns: -2, notNil: false)
    }

    func testInitWithGrid() {
        func expect(grid : [[Int?]], notNil: Bool) {
            let board = Board(withGrid: grid)

            if !notNil {
                XCTAssertNil(board)
                return
            }

            XCTAssertNotNil(board)
            XCTAssertEqual(board?.gridBoard, grid)
        }

        expect(grid: [[1,2], [2,1], [2,1]], notNil: true)
        expect(grid: [[1,2], [2,1], [nil,nil]], notNil: true)
        expect(grid: [[nil,nil], [nil,nil], [nil,nil]], notNil: true)
        expect(grid: [[1], [2,1], [nil,nil]], notNil: false)
    }

    func testIsFull() {
        func expect(grid : [[Int?]], isFull: Bool, notNil: Bool) {
            let board = Board(withGrid: grid)

            if !notNil {
                XCTAssertNil(board)
                return
            }

            XCTAssertNotNil(board)
            XCTAssertEqual(board?.isFull(), isFull)
        }

        expect(grid: [[1,2], [2,1]], isFull: true, notNil: true)
        expect(grid: [[1,nil], [2,nil]], isFull: false, notNil: true)
        expect(grid: [[1,2], [2,nil]], isFull: false, notNil: true)
        expect(grid: [[1,nil], [2,1]], isFull: false, notNil: true)
    }

    func testInsertPiece() {
        func expect(grid : [[Int?]], idPlayer: Int, column: Int, boardResult : BoardResult) {
            var board = Board(withGrid: grid)

            XCTAssertNotNil(board)
            XCTAssertEqual(board?.insertPiece(id: idPlayer, column: column), boardResult)
        }
        
        let grid : [[Int?]] = [[1,2,nil],[nil,nil,nil],[nil,nil,nil]]
        let gridColumnFull : [[Int?]] = [[1,2,nil],[1,nil,nil],[1,nil,nil]]
 
        expect(grid: grid, idPlayer: 2, column: 0, boardResult: .ok)
        expect(grid: grid, idPlayer: 2, column: 2, boardResult: .ok)
        expect(grid: grid, idPlayer: 2, column: -2, boardResult: .failed(reason: .negativeOrOutOfBound))
        expect(grid: grid, idPlayer: 2, column: 7, boardResult: .failed(reason: .negativeOrOutOfBound))
        
        expect(grid: gridColumnFull, idPlayer: 2, column: 0, boardResult: .failed(reason: .columnFull))
        
    }
    
    func testRemovePiece() {
        func expect(grid : [[Int?]]) {
            
        }
    }
    
    func testDescriptionBoard() {
        func expect(grid : [[Int?]], result : String) {
            let board = Board(withGrid: grid)
            
            XCTAssertEqual(board?.description, result)
        }
        
        expect(grid: [[1, 1], [1, 1]], result: "|X|X|\n|X|X|\n")
        expect(grid: [[1, 2], [1, 1]], result: "|X|X|\n|X|O|\n")
        expect(grid: [[nil, 1], [nil, 1]], result: "|-|X|\n|-|X|\n")
        expect(grid: [[nil, nil], [nil, nil]], result: "|-|-|\n|-|-|\n")
    }
    
    func testFullBoard() {
        func expect(grid : [[Int?]], isFull : Bool) {
            let board = Board(withGrid: grid)
                        
            XCTAssertEqual(board?.isFull(), isFull)
        }
        
        expect(grid: [[1, 1, 2], [1, 1, 1], [2, 2, 1]], isFull: true)
        expect(grid: [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]], isFull: false)
        expect(grid: [[nil, 2, nil], [1, 1, nil], [2, 2, nil]], isFull: false)
    }
}
