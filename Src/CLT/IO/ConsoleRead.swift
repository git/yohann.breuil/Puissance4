import Foundation

/// Console reader of the app
public class ConsoleRead : Readable {
    /// Read and return an integer value
    public func readInt() -> Int {
        print("Choose column :")
        if let input = readLine() {
            if let number = Int(input) {
                return number
            }
        }
        return 0
    }
}
